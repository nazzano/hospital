module.exports = {
    devtool: 'source-map',
    entry: {
        main: './src/hospital.js',
        menu: './src/menu.js',
        buttons: './src/buttons.js',
        save_button: './src/save-button.js'
    },
    mode: 'development',
    module: {
        rules: [{
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader'
            }, {
                loader: 'prettier-loader'
            }],
            test: /\.js$/
        }]
    },
    output: {
        filename: '[name].bundle.js',
        path: __dirname + '/dist'
    }
}

