export class People {
  constructor(
    city,
    country,
    state,
    streetName,
    streetNumber,
    birthDay,
    id,
    name,
    surname,
    taxCode,
    bloodyGroup,
    rh
  ) {
    this.birthDay = birthDay;
    this.city = city;
    this.country = country;
    this.id = id;
    this.name = name;
    this.state = state;
    this.streetName = streetName;
    this.streetNumber = streetNumber;
    this.surname = surname;
    this.taxCode = taxCode;
    this.bloodyGroup = bloodyGroup;
    this.rh = rh;
  }
}
