import { addData } from "./functions/initPeople";
import { changeData } from "./functions/initPeople";
import {
  name,
  surname,
  residency,
  streetNum,
  birthDay,
  city,
  region,
  state,
  bloodyGroup,
  rh,
} from "./functions/insert-data";
import { taxCode } from "./functions/tax-code";
import { user } from "./hospital";
import { resPost, resPut } from "./functions/initPeople";

const save = document.getElementById("save");
function response() {
  if (resPost === 201) {
    alert("Salvataggio dei dati effettuato con successo!");
  } else if (resPut === 200) {
    alert("Modifica dei dati effettuata con successo!");
  }
}
save.addEventListener("click", e => {
  e.preventDefault();
  if (user === undefined) {
    const createData = addData(
      city.value,
      state.value,
      region.value,
      residency.value,
      streetNum.value,
      birthDay.value,
      name.value,
      surname.value,
      taxCode.value,
      bloodyGroup.value,
      rh.value
    );
  } else {
    const changeUser = changeData(
      user.id,
      city.value,
      state.value,
      region.value,
      residency.value,
      streetNum.value,
      birthDay.value,
      name.value,
      surname.value,
      taxCode.value,
      bloodyGroup.value,
      rh.value
    );
  }
  response();
});
