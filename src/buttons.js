import { taxCode } from "./functions/tax-code";
import { taxCodeValidator } from "./functions/tax-code";

const back = document.getElementById("back");
const backS = document.getElementById("back1");
const cyan = document.getElementById("cyan");
const green = document.getElementById("green");
const next = document.getElementById("next");
const orange = document.getElementById("orange");
const red = document.getElementById("red");
const white = document.getElementById("white");

red.addEventListener(
  "click",
  function () {
    document.getElementById("step1").hidden = true;
    document.getElementById("step2").hidden = false;
    document.getElementById("codeP").innerHTML = `
      <div class="red" id="btnTry">
          <div class="code-patient" id="CP">Codice Paziente:</div>
          <div class="emergency">Codice Rosso:EMERGENZA</div>
      </div>
      `;
  },
  false
);

orange.addEventListener(
  "click",
  function () {
    document.getElementById("step1").hidden = true;
    document.getElementById("step2").hidden = false;
    document.getElementById("codeP").innerHTML = `
      <div class="orange" id="btnTry">
          <div class="code-patient">Codice Paziente:</div>
          <div class="emergency">Codice Arancione:URGENZA</div>
      </div>
      `;
  },
  false
);

cyan.addEventListener(
  "click",
  function () {
    document.getElementById("step1").hidden = true;
    document.getElementById("step2").hidden = false;
    document.getElementById("codeP").innerHTML = `
      <div class="cyan" id="btnTry">
          <div class="code-patient">Codice Paziente:</div>
          <div class="emergency">Codice Azzurro:URGENZA DIFFERIBILE</div>
      </div>
      `;
    document.getElementById("imp").innerHTML = `
      <div class="mandatory" id="mdt">
        <p>Il Codice Fiscale è obbligatorio per proseguire</p>
      </div>
    `;
  },
  false
);

green.addEventListener(
  "click",
  function () {
    document.getElementById("step1").hidden = true;
    document.getElementById("step2").hidden = false;
    document.getElementById("codeP").innerHTML = `
      <div class="green" id="btnTry">
          <div class="code-patient">Codice Paziente:</div>
          <div class="emergency">Codice Verde:URGENZA MINORE</div>
      </div>
      `;
    document.getElementById("imp").innerHTML = `
      <div class="mandatory" id="mdt">
        <p>Il Codice Fiscale è obbligatorio per proseguire</p>
      </div>
    `;
  },
  false
);

white.addEventListener(
  "click",
  function () {
    document.getElementById("step1").hidden = true;
    document.getElementById("step2").hidden = false;
    document.getElementById("codeP").innerHTML = `
      <div class="white" id="btnTry">
          <div class="code-patient">Codice Paziente:</div>
          <div class="emergency">Codice Bianco:NON URGENZA</div>
      </div>
      `;
    document.getElementById("imp").innerHTML = `
      <div class="mandatory" id="mdt">
        <p>Il Codice Fiscale è obbligatorio per proseguire</p>
      </div>
    `;
  },
  false
);

const mandatory = () => {
  if (taxCode.value.length === taxCodeValidator(taxCode.value)) {
    let error = taxCode.nextElementSibling;
    if (error) {
      error.remove();
    }
    return true;
  }
  if (taxCodeValidator(taxCode.value)) {
    return false;
  }

  return true;
};

back.addEventListener("click", function () {
  document.getElementById("step1").hidden = false;
  document.getElementById("step2").hidden = true;
  document.getElementById("step3").hidden = true;
  document.getElementById("btnTry").remove();
  if (cyan || green || white) {
    document.getElementById("mdt").remove();
  }
});

next.addEventListener("click", function () {
  const cyan1 = document.querySelector('div[class="cyan"]');
  const green1 = document.querySelector('div[class="green"]');
  const white1 = document.querySelector('div[class="white"]');

  if ((cyan1 || green1 || white1) && !mandatory()) {
    return;
  }

  document.getElementById("step1").hidden = true;
  document.getElementById("step2").hidden = true;
  document.getElementById("step3").hidden = false;
});

backS.addEventListener("click", function () {
  document.getElementById("step1").hidden = true;
  document.getElementById("step2").hidden = false;
  document.getElementById("step3").hidden = true;
});

document
  .querySelector('form[name="specifiche"]')
  .addEventListener("submit", function (e) {
    e.preventDefault();
  });
