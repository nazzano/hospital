import "core-js/stable";
import "regenerator-runtime/runtime";
import { getPeople } from "./functions/initPeople";
import { insertData } from "./functions/insert-data";
import { taxCodeValidator } from "./functions/tax-code";
import { taxCode } from "./functions/tax-code";

export let user;

const getInfo = async () => {
  const data = await getPeople();
  return data;
};

getInfo().then(p => {
  taxCode.addEventListener("keyup", f => {
    user = insertData(f, p);
    taxCodeValidator(taxCode.value);
  });
});

$(document).on("ready", function () {
  $(".js-example-basic-multiple").select2({});
});

$(".js-example-responsive").select2({
  width: "resolve",
});

$(".js-example-tags").select2({
  tags: true,
});

$(".js-example-basic-multiple").select2({
  placeholder: {
    id: "-1",
    text: "Farmaci assunti di recente: ",
  },
});

const date = document.getElementById("date");

setInterval(function () {
  date.innerText =
    "Data:" +
    new Date().toLocaleDateString("it-IT") +
    " Ora:" +
    new Date().toLocaleTimeString("it-IT", {
      hour: "2-digit",
      minute: "2-digit",
    });
}, 200);
