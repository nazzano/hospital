import { Drug } from "../models/drug";

export const getDrugs = async _ => {
  const response = await fetch("http://localhost:3000/drugs");

  const data = await response.json();
  return data.map(e => new Drug(e.id, e.name));
};
