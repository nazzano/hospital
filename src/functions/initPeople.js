import { People } from "../models/people";
export const getPeople = async _ => {
  const response = await fetch("http://localhost:3000/people");

  const data = await response.json();
  return data.map(
    e =>
      new People(
        e.address.city,
        e.address.country,
        e.address.state,
        e.address.street_name,
        e.address.street_number,
        e.birth_day,
        e.id,
        e.name,
        e.surname,
        e.tax_code,
        e.bloody_group,
        e.rh
      )
  );
};
export let resPost;
export const addData = async (
  city,
  country,
  state,
  streetName,
  streetNumber,
  birthDay,
  name,
  surname,
  taxCode,
  bloodyGroup,
  rh
) => {
  const response = await fetch(`http://localhost:3000/people`, {
    body: JSON.stringify({
      address: {
        city: city,
        country: country,
        state: state,
        street_name: streetName,
        street_number: streetNumber,
      },
      birth_day: birthDay,
      name: name,
      surname: surname,
      tax_code: taxCode,
      bloody_group: bloodyGroup,
      rh: rh,
    }),
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    method: "POST",
  });
  resPost = response.status;
  const data = await response.json();
  return new People(
    data.address.city,
    data.address.country,
    data.address.state,
    data.address.street_name,
    data.address.street_number,
    data.birth_day,
    data.name,
    data.surname,
    data.tax_code,
    data.bloody_group,
    data.rh
  );
};
export let resPut;
export const changeData = async (
  id,
  city,
  country,
  state,
  streetName,
  streetNumber,
  birthDay,
  name,
  surname,
  taxCode,
  bloodyGroup,
  rh
) => {
  const response = await fetch(`http://localhost:3000/people/${id}`, {
    response: JSON.status,
    body: JSON.stringify({
      address: {
        city: city,
        country: country,
        state: state,
        street_name: streetName,
        street_number: streetNumber,
      },
      birth_day: birthDay,
      name: name,
      surname: surname,
      tax_code: taxCode,
      bloody_group: bloodyGroup,
      rh: rh,
    }),
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    method: "PUT",
  });
  resPut = response.status;
  const data = await response.json();
  return new People(
    id,
    data.address.city,
    data.address.country,
    data.address.state,
    data.address.street_name,
    data.address.street_number,
    data.birth_day,
    data.name,
    data.surname,
    data.tax_code,
    data.bloody_group,
    data.rh
  );
};
