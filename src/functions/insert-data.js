import { taxCodeValidator } from "./tax-code";

export const name = document.getElementById("name");
export const surname = document.getElementById("surname");
export const residency = document.getElementById("residency");
export const streetNum = document.getElementById("street-num");
export const birthDay = document.getElementById("birth-day");
export const state = document.getElementById("state");
export const region = document.getElementById("region");
export const city = document.getElementById("city");
export const bloodyGroup = document.getElementById("bloody-group");
export const rh = document.getElementById("rh");
export const important = document.getElementById("imp");

export function insertData(e, patient) {
  const autofill = patient.find(element => e.target.value === element.taxCode);

  if (taxCodeValidator(e) && autofill != undefined) {
    name.value = autofill.name;
    surname.value = autofill.surname;
    state.value = autofill.state;
    region.value = autofill.country;
    city.value = autofill.city;
    residency.value = autofill.streetName;
    streetNum.value = autofill.streetNumber;
    birthDay.value = autofill.birthDay;
    bloodyGroup.value = autofill.bloodyGroup;
    rh.value = autofill.rh;
    important.remove();
  } else {
    name.value = "";
    surname.value = "";
    state.value = "";
    region.value = "";
    city.value = "";
    residency.value = "";
    streetNum.value = "";
    birthDay.value = "";
    bloodyGroup.value = "Gruppo Sanguigno";
    rh.value = "RH";
  }
  return autofill;
}
